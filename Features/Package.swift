// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Features",
    platforms: [
      .iOS(.v15),
    ],
    products: [
        .library(
            name: "DesignSystem",
            targets: ["DesignSystem"]),
        .library(
            name: "AppFeature",
            targets: ["AppFeature"]),
        .library(
            name: "LoginFeature",
            targets: ["LoginFeature"]),
        .library(
            name: "MainFeature",
            targets: ["MainFeature"]),
        .library(
            name: "HomeFeature",
            targets: ["HomeFeature"]),
        .library(
            name: "SendFeature",
            targets: ["SendFeature"])
    ],
    dependencies: [
        .package(name: "Model", path: "./Model"),
        .package(url: "https://github.com/pointfreeco/swift-composable-architecture", .upToNextMinor(from: "1.10.4")),
        .package(url: "https://github.com/mac-cain13/R.swift.git", from: "7.4.0")
    ],
    targets: [
        .target(
            name: "DesignSystem",
            dependencies: [
                .product(name: "RswiftLibrary", package: "R.swift"),
//                .product(name: "Utilities", package: "Model"),
            ],
            plugins: [.plugin(name: "RswiftGeneratePublicResources", package: "R.swift")]),
        .target(
            name: "AppFeature",
            dependencies: [
                "LoginFeature",
                "MainFeature",
                .product(name: "SessionManager", package: "Model"),
                .product(name: "LogoutService", package: "Model"),
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
            ]),
        .target(
            name: "LoginFeature",
            dependencies: [
                "DesignSystem",
                .product(name: "BitkoinService", package: "Model"),
                .product(name: "SessionManager", package: "Model"),
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
            ]),
        .target(
            name: "MainFeature",
            dependencies: [
                "DesignSystem",
                "HomeFeature",
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
            ]),
        .target(
            name: "HomeFeature",
            dependencies: [
                "DesignSystem",
                "SendFeature",
                .product(name: "LogoutService", package: "Model"),
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
                .product(name: "BitkoinService", package: "Model"),
                .product(name: "BitkoinRestService", package: "Model"),
            ]),
        .target(
            name: "SendFeature",
            dependencies: [
                "DesignSystem",
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
                .product(name: "BitkoinService", package: "Model"),
                .product(name: "BitkoinRestService", package: "Model"),
            ])
    ]
)
