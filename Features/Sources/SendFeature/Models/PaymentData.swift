//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

public struct ConfirmPaymentData: Equatable {
    let fromWalletAddress: String
    let toWalletAddress: String
    let amount: NSDecimalNumber
    let comission: NSDecimalNumber
}
