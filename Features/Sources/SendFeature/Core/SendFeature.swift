//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import ComposableArchitecture
import UIKit

@Reducer
public struct SendFeature: Reducer {
    @Dependency(\.dismiss) var dismiss
    @Dependency(\.mainQueue) var mainQueue
    @Dependency(SendScreenService.self) var sendScreenService
    
    public init() {}
    
    @ObservableState
    public struct State: Equatable {
        public init() {}
        
        var canContinue: Bool {
            switch paymentState {
            case .enterData:
                return !walletAddress.isEmpty && !amountToSend.isEmpty
            case .validateData:
                return false
            case .needConfirmPayment, .error, .success:
                return true
            }
        }
        var walletAddress: String = ""
        var amountToSend: String = "0.0001"
        var isLoading: Bool = true
        var paymentState: PaymentState = .enterData
        var focusedField: SendView.FocusedField?
        
        enum PaymentState: Equatable {
            case enterData
            case validateData
            case error(String)
            case needConfirmPayment(ConfirmPaymentData)
            case success(String)
        }
        
    }
    
    public enum Action: ViewAction {
        case view(View)
        
        case validateData
        case validateDataError(String)
        case getConfirmData(ConfirmPaymentData)
        
        case confirmPayment
        
        case successPayment(String)
        
        case delegate(Delegate)
        
        @CasePathable
        public enum View: BindableAction {
            case binding(BindingAction<State>)
            case closeButtonTapped
            case continueBtnTapped
            case walletAddressUpdated(String)
            case amountToSendUpdated(String)
            case pasteFromClipboardToAddress
            case openTransactionInBrowser
        }
        
        public enum Delegate {
            case successPayment
        }
    }
    
    public var body: some Reducer<State, Action> {
        BindingReducer(action: \.view)
        Reduce { state, action in
            switch action {
            case .view(let action):
                switch action {
                case .binding:
                    return .none
                case .closeButtonTapped:
                    return .run { action in
                        await dismiss()
                    }
                    
                case .continueBtnTapped:
                    switch state.paymentState {
                    case .enterData, .error:
                        state.focusedField = nil
                        return .send(.validateData)
                    case .validateData:
                        return .none
                    case .needConfirmPayment:
                        if state.focusedField != nil {
                            state.focusedField = nil
                            return .none
                        }
                        return .send(.confirmPayment)
                    case .success:
                        return .send(.view(.closeButtonTapped))
                    }
                    
                    
                case .walletAddressUpdated(let value):
                    
                    guard state.walletAddress != value else { return .none }
                    
                    state.paymentState = .enterData
                    state.walletAddress = value
                    
                    return .cancel(id: CancelID.cancel)
                    
                case .amountToSendUpdated(let value):
                    
                    guard state.amountToSend != value else { return .none }
                    
                    state.paymentState = .enterData
                    state.amountToSend = value
                    
                    return .cancel(id: CancelID.cancel)
                    
                case .pasteFromClipboardToAddress:
                    guard let value = UIPasteboard.general.string else { return .none }
                    return .run {[value] action in
                        await action(.view(.walletAddressUpdated(value)))
                    }
                    
                case .openTransactionInBrowser:
                    guard case .success(let txId) = state.paymentState,
                          let url = URL(string: "https://mempool.space/signet/tx/")?.appendingPathComponent(txId),
                          UIApplication.shared.canOpenURL(url)
                    else { return .none }
                    
                    UIApplication.shared.open(url)
                    return .none
                }
            case .validateData:
                state.paymentState = .validateData
                
                return .run {[address = state.walletAddress, amountStr = state.amountToSend] action in
                    await withTaskCancellation(id: CancelID.cancel, cancelInFlight: true) {
                        guard !address.isEmpty else { await action(.validateDataError("invalid address")); return }
                        guard !amountStr.isEmpty else { await action(.validateDataError("invalid amount")); return }
                        
                        let amount: NSDecimalNumber = .init(string: amountStr)
                        
                        guard amount.compare(.init(value: 0)) == .orderedDescending else {
                            await action(.validateDataError("invalid amount"))
                            return
                        }
                        
                        guard let confirmData = try? await sendScreenService.generateConfirmPaymentData(.init(amount: amount, walletTo: address)) else {
                            await action(.validateDataError("generateConfirmPaymentData error"))
                            return
                        }
                        
                        await action(.getConfirmData(confirmData))
                    }
                }
                
            case .getConfirmData(let data):
                state.paymentState = .needConfirmPayment(data)
                return .none
                
            case .validateDataError(let error):
                state.paymentState = .error(error)
                return .none
                
            case .confirmPayment:
                if case .needConfirmPayment(let data) = state.paymentState {
                    return .run { action in
                        do {
                            let txId = try await sendScreenService.confirmPayment(data)
                            await action(.successPayment(txId))
                        } catch {
                            await action(.validateDataError(error.localizedDescription))
                        }
                    }.animation()
                } else {
                    return .none
                }
            case .successPayment(let txId):
                state.paymentState = .success(txId)
                return .send(.delegate(.successPayment))
                
            case .delegate:
                return .none
            }
        }
        
    }
}

enum CancelID {
    case cancel
}
