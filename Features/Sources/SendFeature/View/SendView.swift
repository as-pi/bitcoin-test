//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import SwiftUI
import DesignSystem
import ComposableArchitecture

@ViewAction(for: SendFeature.self)
public struct SendView: View {
    @Perception.Bindable public var store: StoreOf<SendFeature>
    
    @FocusState var focusedField: FocusedField?

    public enum FocusedField: Equatable {
        case wallet
        case amount
    }
    
    public init(store: StoreOf<SendFeature>) {
        self.store = store
    }

    public var body: some View {
        WithPerceptionTracking(content: {
            
            let isSuccess: Bool = {
                if case .success = store.paymentState {
                    return true
                }
                return false
            }()
            
            VStack {
                if isSuccess {
                    sendSuccessView
                    .navigationStyle(title: Text(""), navTitleStyle: .inline, hasBackButton: false, trailing: {
                        Button {
                            send(.closeButtonTapped)
                        } label: {
                            Image(systemName: "xmark")
                        }
                    })
                } else {
                    contentView
                        .navigationStyle(title: Text(R.string.strings.sendFeatureTitle()), navTitleStyle: .inline, hasBackButton: false, trailing: {
                        Button {
                            send(.closeButtonTapped)
                        } label: {
                            Image(systemName: "xmark")
                        }
                    })
                }
            }
            
            .onTapGesture {
                focusedField = nil
            }
            .synchronize($store.focusedField, $focusedField)
        })
    }
    
}

#Preview {
    SendView(store: Store(initialState: SendFeature.State(), reducer: {
        SendFeature()
    }))
}
