//
//  File.swift
//  
//
//  Created by aleksey on 18.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture

extension SendView {
    
    @ViewBuilder
    var contentByStateView: some View {
        if focusedField == nil {
            VStack {
                switch store.paymentState {
                case .enterData, .success:
                    Spacer()
                case .error(let value):
                    createErrortView(error: value)
                case .validateData:
                    Spacer()
                    
                    LoadingSpinner()
                    
                    Spacer()
                case .needConfirmPayment(let data):
                    createNeedConfirmPaymentView(data: data)
                }
            }
        } else {
            Spacer()
        }
    }
    
    func createErrortView(error: String) -> some View {
        VStack(alignment: .leading) {
            VStack {
                VStack(alignment: .leading, spacing: 16) {
                    Text("\(R.string.strings.error()):")
                        .font(AppTheme.Fonts.bodyBold)
                        .foregroundStyle(AppTheme.Colors.constantBlack)
                        .frame(maxWidth: .infinity, alignment: .center)
                    
                    Text(error)
                        .font(AppTheme.Fonts.bodyBold)
                        .foregroundStyle(AppTheme.Colors.constantBlack)
                }
                .padding(16)
            }
            .frame(maxWidth: .infinity)
            .background(AppTheme.Colors.backgroundSecondary)
            .cornerRadius(16)
            
            Spacer()
        }
        .frame(maxWidth: .infinity)
    }
    
    func createNeedConfirmPaymentView(data: ConfirmPaymentData) -> some View {
        VStack(alignment: .leading) {
            VStack {
                VStack(alignment: .leading, spacing: 16) {
                    Text(R.string.strings.sendFeatureCheckData)
                        .font(AppTheme.Fonts.bodyBold)
                        .foregroundStyle(AppTheme.Colors.constantBlack)
                        .frame(maxWidth: .infinity, alignment: .center)
                    
                    VStack(alignment: .leading) {
                        Text(R.string.strings.sendFeatureSender)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                        
                        Text(data.fromWalletAddress)
                            .font(AppTheme.Fonts.body)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                    }
                    
                    VStack(alignment: .leading) {
                        Text(R.string.strings.sendFeatureRecipient)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                        
                        Text(data.toWalletAddress)
                            .font(AppTheme.Fonts.body)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                    }
                    
                    
                    HStack(spacing: 8)  {
                        Text(R.string.strings.sendFeatureAmount)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                        
                        Text(data.amount.btcAmount.toBtcAmount())
                            .font(AppTheme.Fonts.body)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                    }
                    
                    HStack(spacing: 8) {
                        Text(R.string.strings.sendFeatureFee)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                        
                        Text(data.comission.btcAmount.toBtcAmount())
                            .font(AppTheme.Fonts.body)
                            .foregroundStyle(AppTheme.Colors.constantBlack)
                    }
                    
                    
                }
                .padding(16)
            }
            .frame(maxWidth: .infinity)
            .background(AppTheme.Colors.backgroundSecondary)
            .cornerRadius(16)
            
            Spacer()
        }
        .frame(maxWidth: .infinity)
    }
    
}
