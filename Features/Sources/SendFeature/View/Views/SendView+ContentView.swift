//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import SwiftUI
import DesignSystem
import ComposableArchitecture

extension SendView {
    
    var contentView: some View {
        VStack(spacing: 16) {
            
            headerView
            
            contentByStateView
            
            bottomView
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
    }
    
}
