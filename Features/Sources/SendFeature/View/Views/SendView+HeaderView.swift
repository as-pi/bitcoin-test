//
//  File.swift
//  
//
//  Created by aleksey on 18.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture

extension SendView {
    
    var headerView: some View {
        VStack(spacing: 16) {
            Text(R.string.strings.sendFeatureRecipient)
                .font(AppTheme.Fonts.bodyBold)
            
            HStack {
                TextField(text: $store.walletAddress.sending(\.view.walletAddressUpdated), label: {
                    Text("")
                })
                .font(AppTheme.Fonts.body)
                .focused($focusedField, equals: .wallet)
                .padding(16)
                
                Button {
                    send(.pasteFromClipboardToAddress)
                } label: {
                    
                    VStack {
                        Image(systemName: "arrow.up.doc.on.clipboard")
                            .foregroundStyle(AppTheme.Colors.textPrimary)
                    }
                }
                .padding(.trailing, 16)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(AppTheme.Colors.controlPrimaryActive, lineWidth: 0.5)
            )
            
            Text(R.string.strings.sendFeatureAmountToSend())
                .font(AppTheme.Fonts.bodyBold)
            
            HStack {
                TextField(text: $store.amountToSend.sending(\.view.amountToSendUpdated), label: {
                    Text("1")
                })
                .font(AppTheme.Fonts.body)
                .keyboardType(.decimalPad)
                .focused($focusedField, equals: .amount)
                .padding(16)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(AppTheme.Colors.controlPrimaryActive, lineWidth: 0.5)
            )
        }
    }
    
}
