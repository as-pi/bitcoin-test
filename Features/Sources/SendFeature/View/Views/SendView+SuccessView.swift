//
//  File.swift
//  
//
//  Created by aleksey on 19.06.2024.
//

import Foundation
import DesignSystem
import ComposableArchitecture
import SwiftUI

extension SendView {
    
    var sendSuccessView: some View {
        VStack(spacing: 0) {
            Spacer()
            Image(systemName: "checkmark.circle")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .foregroundColor(AppTheme.Colors.brand)
                .frame(width: 64)
            
            Text(R.string.strings.sendFeatureSuccessPayment)
                .font(AppTheme.Fonts.headerBold)
                .foregroundStyle(AppTheme.Colors.textPrimary)
            
            if case .success(let txId) = store.paymentState {
                VStack {
                    Text("TxID:")
                        .font(AppTheme.Fonts.bodyBold)
                        .foregroundStyle(AppTheme.Colors.textPrimary)
                    
                    Text(txId)
                        .font(AppTheme.Fonts.body)
                        .foregroundStyle(AppTheme.Colors.accentActive)
                        .multilineTextAlignment(.center)
                }
                .padding(.top, 16)
                .onTapGesture {
                    send(.openTransactionInBrowser)
                }
            }
            
            Spacer()
            
            Button(R.string.strings.ok()) {
                send(.continueBtnTapped)
            }
            .buttonStyle(.simpleStyle(.primary))
            .disabled(!store.canContinue)
        }
        .padding(16)
    }
}
