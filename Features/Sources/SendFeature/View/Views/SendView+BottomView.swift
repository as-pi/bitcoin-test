//
//  File.swift
//  
//
//  Created by aleksey on 18.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture

extension SendView {
    
    var bottomView: some View {
        
        Button(R.string.strings.continue()) {
            send(.continueBtnTapped)
        }
        .buttonStyle(.simpleStyle(.primary))
        .disabled(!store.canContinue)
        
    }
    
}
