//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import BitkoinService
import ComposableArchitecture
import BitkoinRestService

public struct SendScreenService {
    
    public struct DataForGeneratePayment {
        let amount: NSDecimalNumber
        let walletTo: String
    }
    
    public var generateConfirmPaymentData: @Sendable (DataForGeneratePayment) async throws -> ConfirmPaymentData
    public var confirmPayment: @Sendable (ConfirmPaymentData) async throws -> String
    
}

extension SendScreenService {
    public static let live: SendScreenService = {
        
        @Dependency(BitkoinService.self) var bitkoinService
        
        return Self (
            generateConfirmPaymentData: { data in
                let outputWallet = try await bitkoinService.inputAddresses().first
                
                guard let outputWallet = outputWallet else { throw NSError(domain: "", code: 1)}
                
                let utxoData = try await BitkoinRestService.shared.getUTXO(for:outputWallet)
                
                let fee = NSDecimalNumber(value: try await bitkoinService.calculateComission(utxoData.count)).dividing(by: .init(value: 1_000_000_00))
                
                return .init(
                    fromWalletAddress: outputWallet,
                    toWalletAddress: data.walletTo,
                    amount: data.amount,
                    comission: fee)
            },
            confirmPayment: {data in
                
                var addressUtxo: [BitkoinService.GenerateHexData.WalletUtxoData] = []
                
                let utxoData = try await BitkoinRestService.shared.getUTXO(for: data.fromWalletAddress)
                for item in utxoData {
                    addressUtxo.append(.init(
                        balance: item.value,
                        txID: item.txid,
                        index: item.vout))
                }
                
                let hex = try await bitkoinService.generateHexForTransaction(.init(
                    fromWallet: data.fromWalletAddress,
                    toWallet: data.toWalletAddress,
                    amount: data.amount.multiplying(by: .init(value: 1_000_000_00)).uint64Value,
                    fee: data.comission.multiplying(by: .init(value: 1_000_000_00)).uint64Value,
                    walletUtxoData: addressUtxo
                ))
                
                return try await BitkoinRestService.shared.createTransaction(hex: hex)
            }
        )
    }()
    
    enum ServiceError: Error {
        case invalidAmount
    }
}

extension SendScreenService: DependencyKey {
    public static let liveValue: SendScreenService = .live
}
