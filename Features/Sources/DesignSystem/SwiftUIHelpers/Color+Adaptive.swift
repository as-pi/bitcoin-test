//
//  Color+Adaptive.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

extension UIColor {
  convenience init(light: UIColor, dark: UIColor) {
    self.init { traitCollection in
      switch traitCollection.userInterfaceStyle {
      case .light, .unspecified:
        return light
      case .dark:
        return dark
      @unknown default:
        return light
      }
    }
  }
}

extension Color {
  init(light: UIColor, dark: UIColor) {
    self.init(UIColor(light: light, dark: dark))
  }
}
