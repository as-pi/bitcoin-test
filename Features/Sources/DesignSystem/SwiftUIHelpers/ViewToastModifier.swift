//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

struct ViewToastModifier<PanelContent: View>: ViewModifier {
    @Binding var isPresented: Bool
    let content: () -> PanelContent
    @State private var alpha: Double = 1
    
    init(
        isPresented: Binding<Bool>,
        content: @escaping () -> PanelContent
    ) {
        self.content = content
        self._isPresented = isPresented
    }

    func body(content: Content) -> some View {
        content.overlay(self.$isPresented.wrappedValue ? panelContent() : nil)
    }
    
    private func panelContent() -> some View {
        VStack {
            content()
                .background(AppTheme.Colors.backgroundSecondary)
                .cornerRadius(16)
                .opacity(alpha)
                .task {
                    alpha = 1
                    withAnimation (.easeInOut(duration: 1)) {
                        alpha = 0
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        isPresented = false
                    })
                }
        }
        .frame(maxHeight: .infinity, alignment: .bottom)
    }
}
extension View {
    public func toast(
        isPresented: Binding<Bool>,
        @ViewBuilder content: @escaping () -> some View
    ) -> some View {
        return modifier(ViewToastModifier(isPresented: isPresented, content: content))
    }
}
