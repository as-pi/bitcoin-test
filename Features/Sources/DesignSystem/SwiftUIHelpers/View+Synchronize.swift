//
//  View+Synchronize.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

public extension View {
    func synchronize<Value: Equatable>(
      _ first: Binding<Value>,
      _ second: FocusState<Value>.Binding
    ) -> some View {
      self
        .onChange(of: first.wrappedValue) { second.wrappedValue = $0 }
        .onChange(of: second.wrappedValue) { first.wrappedValue = $0 }
    }
}
