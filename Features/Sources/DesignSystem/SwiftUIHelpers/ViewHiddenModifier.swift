//
//  ViewHiddenModifier.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

struct ViewHiddenModifier: ViewModifier {
    let isHidden: Bool

    func body(content: Content) -> some View {
        if isHidden {
            content.hidden()
        } else {
            content
        }
    }
}

extension View {
    public func isHidden(_ isHidden: Bool) -> some View {
        self.modifier(ViewHiddenModifier(isHidden: isHidden))
    }
}
