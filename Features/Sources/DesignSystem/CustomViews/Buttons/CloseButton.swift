//
//  CloseButton.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

extension View {
    public func closeButton(color: Color = AppTheme.Colors.controlPrimaryActive, dismissAction: @escaping () -> Void) -> some View {
        HStack {
            Spacer()
            Button {
                dismissAction()
            } label: {
                Image(systemName: "xmark")
                    .font(AppTheme.Fonts.header2)
                    .foregroundColor(color)
                    .frame(width: 40, height: 35, alignment: .topTrailing)
                    .contentShape(Rectangle())
            }
        }
    }
}
