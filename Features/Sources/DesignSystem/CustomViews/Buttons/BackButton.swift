//
//  BackButton.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

extension View {
    public func backButton(color: Color = AppTheme.Colors.controlPrimaryActive, dismissAction: @escaping () -> Void) -> some View {
        HStack {
            Button {
                dismissAction()
            } label: {
                Image(systemName: "arrow.left")
                    .font(AppTheme.Fonts.header2)
                    .foregroundColor(color)
                    .frame(width: 40, height: 30, alignment: .topLeading)
                    .contentShape(Rectangle())
            }
            Spacer()
        }
    }
}
