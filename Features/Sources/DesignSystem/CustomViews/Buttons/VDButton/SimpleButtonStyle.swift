//
//  SimpleButton.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

public enum SimpleButtonStyle {
    case primary
    case secondary
}

public enum SimpleButtonSize {
    case medium
    case small
    case extraSmall
}


public struct SimpleButton: ButtonStyle {
    @Environment(\.isEnabled) private var isEnabled: Bool
    
    let style: SimpleButtonStyle
    let size: SimpleButtonSize
    
    let isLoading: Bool
    
    var height: CGFloat {
        switch size {
        case .medium:
            return 52
        case .small:
            return 44
        case .extraSmall:
            return 36
        }
    }
    
    var foregroundColor: Color {
//        switch style {
//        case .primary:
//            return isEnabled ? AppTheme.Colors.textPrimary : AppTheme.Colors.controlPrimaryInactive
//        case .secondary:
        return isEnabled ? AppTheme.Colors.textPrimary : AppTheme.Colors.textSecondary
//        }
    }
    
    var backgroundColor: Color {
        switch style {
        case .primary:
            return isEnabled ? AppTheme.Colors.brand : AppTheme.Colors.backgroundSecondary
        case .secondary:
            return AppTheme.Colors.backgroundPrimary
        }
    }
    
    var lineColor: Color {
        switch style {
        case .primary:
            return .clear
        case .secondary:
            return AppTheme.Colors.controlPrimaryActive
        }
    }
    
    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .isHidden(isLoading)
            .padding(.horizontal)
            .frame(height: height)
            .frame(maxWidth: .infinity)
            .background(backgroundColor)
            .opacity(configuration.isPressed ? 0.7 : 1)
            .cornerRadius(size == .extraSmall ? 14 : 16)
            .foregroundColor(foregroundColor)
            .font(AppTheme.Fonts.bodyBold)
            .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
            .allowsHitTesting(!isLoading)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(lineColor, lineWidth: 0.5)
            )
            .overlay {
                if isLoading {
                    ButtonSpinner(buttonStyle: style)
                }
            }
        
    }
}

extension ButtonStyle where Self == SimpleButton {
    public static func simpleStyle(_ style: SimpleButtonStyle, size: SimpleButtonSize = .medium, isLoading: Bool = false) -> Self {
        Self(
            style: style,
            size: size,
            isLoading: isLoading
        )
    }
}
