//
//  ButtonSpinner.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

struct ButtonSpinner: View {
    @State private var isLoading = false
    let buttonStyle: SimpleButtonStyle
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(buttonStyle == .primary ? AppTheme.Colors.controlPrimaryInverted : AppTheme.Colors.controlPrimaryInactive, lineWidth: 3).opacity(0.4)
                .frame(width: 20, height: 20)
            Circle()
                .trim(from: 0, to: 0.7)
                .stroke(buttonStyle == .primary ? AppTheme.Colors.controlPrimaryInverted : AppTheme.Colors.brand, lineWidth: 3)
                .frame(width: 20, height: 20)
                .rotationEffect(Angle(degrees: isLoading ? 360 : 0))
                .animation(Animation.linear(duration: 1)
                    .repeatForever(autoreverses: false), value: isLoading)
                .onAppear {
                    DispatchQueue.main.async {
                        self.isLoading = true
                    }
                }
        }
    }
}
