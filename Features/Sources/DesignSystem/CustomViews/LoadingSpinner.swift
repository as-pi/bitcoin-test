//
//  LoadingSpinner.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

public struct LoadingSpinner: View {
    
    public init() {}
    
    @State private var isLoading = false
    public var body: some View {
        ZStack {
            Circle()
                .trim(from: 0, to: 0.7)
                .stroke(AppTheme.Colors.controlPrimaryActive, lineWidth: 5)
                .frame(width: 36, height: 36)
                .rotationEffect(Angle(degrees: isLoading ? 360 : 0))
                .animation(Animation.linear(duration: 1)
                    .repeatForever(autoreverses: false), value: isLoading)
                .onAppear {
                    DispatchQueue.main.async {
                        self.isLoading = true
                    }
                }
        }
    }
}

struct LoadingSpinner_Previews: PreviewProvider {
    static var previews: some View {
        LoadingSpinner()
    }
}
