//
//  NavigationBar.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

public enum NavTitleStyle {
    case inline
    case large
}

extension View {
    public func navigationStyle<Title: View, Trailing: View>(
        backgroundColor: Color = AppTheme.Colors.backgroundPrimary,
        title: Title,
        navTitleStyle: NavTitleStyle = .large,
        itemBarHidden: Bool = false,
        hasBackButton: Bool = true,
        @ViewBuilder trailing: () -> Trailing = { EmptyView() },
        onDismiss: @escaping () -> Void = {},
        dismissOnBackTapped: Bool = true
    ) -> some View {
        NavigationBar(
            backgroundColor: backgroundColor,
            content: self,
            title: title,
            navTitleStyle: navTitleStyle,
            itemBarHidden: itemBarHidden,
            hasBackButton: hasBackButton,
            trailing: trailing(),
            onDismiss: onDismiss,
            dismissOnBackTapped: dismissOnBackTapped
        )
    }

    public func navigationBackButton(
        backgroundColor: Color = AppTheme.Colors.backgroundPrimary,
        onDismiss: @escaping () -> Void = {}
    ) -> some View {
        self.navigationStyle(
            backgroundColor: backgroundColor,
            title: EmptyView(),
            navTitleStyle: .inline,
            trailing: { EmptyView() },
            onDismiss: onDismiss,
            dismissOnBackTapped: true
        )
    }

    public func navigationLargeTitle<Title: View>(
        backgroundColor: Color = AppTheme.Colors.backgroundPrimary,
        title: Title
    ) -> some View {
        self.navigationStyle(
            backgroundColor: backgroundColor,
            title: title,
            itemBarHidden: true,
            trailing: { EmptyView() },
            onDismiss: {},
            dismissOnBackTapped: true
        )
    }
}

private struct NavigationBar<Title: View, Content: View, Trailing: View>: View {
    let backgroundColor: Color
    let content: Content
    @Environment(\.dismiss) var dismiss
    let title: Title
    let navTitleStyle: NavTitleStyle
    let itemBarHidden: Bool
    let hasBackButton: Bool
    let trailing: Trailing
    let onDismiss: () -> Void
    let dismissOnBackTapped: Bool

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            VStack(alignment: .leading, spacing: itemBarHidden ? 14 : 26) {
                ZStack {
                    self.title
                        .font(.system(size: 17, weight: .semibold))
                        .isHidden(navTitleStyle != .inline)

                    HStack {
                        if hasBackButton {
                            Button(action: self.dismissAction) {
                                Image(systemName: "arrow.left")
                                    .font(AppTheme.Fonts.header2)
                            }
                        }
                        Spacer()
                        self.trailing
                    }
                }
                .foregroundColor(AppTheme.Colors.textPrimary)
                .isHidden(itemBarHidden)

                if navTitleStyle == .large {
                    self.title
                        .font(AppTheme.Fonts.headerBold)
                }
            }
            .padding([.top, .horizontal])
            .padding(.bottom, navTitleStyle == .large ? 12 : 10)

            self.content
        }
        .background(self.backgroundColor.ignoresSafeArea())
        .navigationBarHidden(true)
    }

    func dismissAction() {
        self.onDismiss()
        if self.dismissOnBackTapped {
            self.dismiss()
        }
    }
}
