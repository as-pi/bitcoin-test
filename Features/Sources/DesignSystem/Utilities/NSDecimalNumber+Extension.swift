//
//  File.swift
//  
//
//  Created by aleksey on 18.06.2024.
//

import Foundation

public extension NSDecimalNumber {
    var btcAmount: UInt64 {
        return self.multiplying(by: .init(value: 1_000_000_00)).uint64Value
    }
}
