//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

public extension UInt64 {
    func toBtcAmount() -> String {
        let value: NSDecimalNumber = .init(value: self).dividing(by: .init(value: 1_000_000_00))
        let formatter: NumberFormatter = .init()
        formatter.minimumFractionDigits = 8
        return formatter.string(from: value) ?? ""
    }
}
