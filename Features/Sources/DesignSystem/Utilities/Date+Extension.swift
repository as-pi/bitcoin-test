//
//  File.swift
//  
//
//  Created by aleksey on 18.06.2024.
//

import Foundation

public extension Date {
    func removeTimeStamp() -> Date {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: self)) else {
            return self
        }
        return date
    }
}
