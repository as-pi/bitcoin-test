//
//  Fonts.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI

extension AppTheme {
    public enum Fonts {
//        public static let caption1 = Font.system(size: 12, weight: .regular)
//        public static let caption1Bold = Font.system(size: 12, weight: .semibold)
//        
//        public static let bodyXS = Font.system(size: 11, weight: .regular)
//        public static let bodyXSBold = Font.system(size: 11, weight: .semibold)
//        
        public static let bodyS = Font.system(size: 13, weight: .regular)
//        public static let bodySBold = Font.system(size: 13, weight: .semibold)
//        
//        public static let bodyM = Font.system(size: 14, weight: .regular)
//        public static let bodyMBold = Font.system(size: 14, weight: .semibold)
//        
        public static let body = Font.system(size: 15, weight: .regular)
        public static let bodyBold = Font.system(size: 15, weight: .semibold)
//        
//        public static let callout = Font.system(size: 16, weight: .regular)
//        public static let calloutBold = Font.system(size: 16, weight: .semibold)
//        
//        public static let header3 = Font.system(size: 18, weight: .regular)
//        public static let header3Bold = Font.system(size: 18, weight: .semibold)
//        
//        public static let title3 = Font.system(size: 20, weight: .regular)
//        public static let title3Bold = Font.system(size: 20, weight: .semibold)
//        
        public static let header2 = Font.system(size: 22, weight: .regular)
//        public static let header2Bold = Font.system(size: 22, weight: .semibold)
//        
//        public static let header1 = Font.system(size: 28, weight: .regular)
//        public static let header1Bold = Font.system(size: 28, weight: .semibold)
//        
//        public static let header = Font.system(size: 32, weight: .regular)
        public static let headerBold = Font.system(size: 32, weight: .semibold)
//        
//        public static let display = Font.system(size: 38, weight: .regular)
//        public static let displayBold = Font.system(size: 38, weight: .semibold)
    }
}
