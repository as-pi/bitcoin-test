//
//  AppFeature.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import ComposableArchitecture
import LoginFeature
import MainFeature
import SessionManager
import LogoutService
import UIKit

@Reducer
public struct AppFeature: Reducer {
    @Dependency(SessionManager.self) var sessionManager
    @Dependency(LogoutService.self) var logoutService

    @Reducer(state: .equatable)
    public enum Destination {
        case login(LoginFeature)
        case main(MainFeature)
    }

    @ObservableState
    public struct State: Equatable {
        var appDelegate: AppDelegateReducer.State
        @Presents var destination: Destination.State?

        public init(
            appDelegate: AppDelegateReducer.State = AppDelegateReducer.State(),
            destination: Destination.State? = nil
        ) {
            self.appDelegate = appDelegate
            self.destination = destination
        }
    }

    public enum Action {
        case appDelegate(AppDelegateReducer.Action)
        case appWillEnterForeground
        case destination(PresentationAction<Destination.Action>)
        case sessionSubscription
        case logout
    }

    public init() {}


    public var body: some Reducer<State, Action> {
        Scope(state: \.appDelegate, action: \.appDelegate) {
            AppDelegateReducer()
        }
        Reduce { state, action in
            enum SessionID { case session }

            switch action {
            case .appDelegate(.didFinishLaunching):
                state.destination = sessionManager.isNeedLogin() ? .login(.init()) : .main(.init())
                return .none

            case .appDelegate:
                return .none

            case .appWillEnterForeground:
                return .none
                
            case .destination(.presented(.login(.delegate(let action)))):
                switch action {
                case .success:
                    state.destination = .main(.init())
                }
                return .none
            case .destination(.presented(.login)):
                return .none
                
            case .destination(.presented(.main)):
                return .none
                
            case .sessionSubscription:
                return .run { send in
                    for await isExpired in self.logoutService.sessionStream where isExpired {
                        await send(.logout, animation: .default.speed(2))
//                        await pushNotificationsClient.didLogout()
                        await sessionManager.clearSessionData()
                    }
                }.cancellable(id: SessionID.session)

            case .logout:
                state.destination = .login(.init())
                return .cancel(id: SessionID.session)

            case .destination(.dismiss):
                return .none
            }
        }
        .ifLet(\.$destination, action: \.destination)
    }
}
