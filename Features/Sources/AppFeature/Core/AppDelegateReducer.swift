//
//  AppDelegateReducer.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import ComposableArchitecture
import SessionManager

@Reducer
public struct AppDelegateReducer: Reducer {
    @Dependency(SessionManager.self) var sessionManager

    public struct State: Equatable {
        public init() {}
    }

    public enum Action {
        case didFinishLaunching
    }

    public init() {}

    public var body: some Reducer<State, Action> {
        Reduce { _, action in
            switch action {
            case .didFinishLaunching:
                return .none
            }
        }

    }
}
