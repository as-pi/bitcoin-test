//
//  AppView.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import SwiftUI
import ComposableArchitecture
import DesignSystem
import LoginFeature
import MainFeature
import SessionManager

public struct AppView: View {
    let store: StoreOf<AppFeature>

    public init(store: StoreOf<AppFeature>) {
        self.store = store
    }

    public var body: some View {
        WithPerceptionTracking(content: {
            switch store.destination {
            case .none:
                VStack(spacing: 50) {
                    Image(R.image.bitcoin_logo)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 200)
                    LoadingSpinner()
                }
            case .some(.login):
                if let loginStore = store.scope(state: \.destination?.login,
                                                action: \.destination.login) {
                    NavigationView {
                        LoginView(store: loginStore)
                    }
                }
            case .some(.main):
                if let mainStore = store.scope(state: \.destination?.main,
                                               action: \.destination.main) {
                    MainView(store: mainStore)
                    .task {
                        store.send(.sessionSubscription)
                    }
                }
            }
        })
    }
}

#Preview {
    AppView(store: Store(initialState: AppFeature.State(), reducer: {
        AppFeature()
    }))
}
