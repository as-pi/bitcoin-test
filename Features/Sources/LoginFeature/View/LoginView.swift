//
//  LoginView.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import SwiftUI
import ComposableArchitecture
import DesignSystem

@ViewAction(for: LoginFeature.self)
public struct LoginView: View {
    @Perception.Bindable public var store: StoreOf<LoginFeature>

    public init(store: StoreOf<LoginFeature>) {
        self.store = store
    }

    public var body: some View {
        
        WithPerceptionTracking {
            
            contentView
            
        }
    }
}

#Preview {
    LoginView(store: Store(initialState: LoginFeature.State(), reducer: {
        LoginFeature()
    }))
}
