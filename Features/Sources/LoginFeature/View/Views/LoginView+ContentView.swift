//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem

extension LoginView {
    var contentView: some View {
        VStack {
            
            Text(R.string.strings.loginFeatureGenerateSeedTitle)
                .font(AppTheme.Fonts.header2)
                .frame(maxWidth: .infinity, alignment: .topLeading)
            
            TextEditor(text: $store.seed.sending(\.view.seedTextUpdated))
            
            .frame(height: 200)
            .background(AppTheme.Colors.backgroundPrimary)
            .foregroundStyle(AppTheme.Colors.textPrimary)
            .cornerRadius(16)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(AppTheme.Colors.controlPrimaryActive, lineWidth: 0.5)
            )
            
            Button(R.string.strings.loginFeatureGenerateSeed()) {
                send(.generateButtonTapped)
            }
            .buttonStyle(.simpleStyle(.secondary))
            
            Spacer()
            
            .buttonStyle(.simpleStyle(.primary))
            
            Button(R.string.strings.continue()) {
                send(.continueButtonTapped)
            }
            .buttonStyle(.simpleStyle(.primary))
            .disabled(!store.canContinue)
        }
        .padding()
        .frame(maxHeight: .infinity)
    }
}
