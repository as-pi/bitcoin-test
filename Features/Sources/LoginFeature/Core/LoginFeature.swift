//
//  LoginFeature.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import ComposableArchitecture
import SessionManager
import BitkoinService

@Reducer
public struct LoginFeature: Reducer {
    @Dependency(SessionManager.self) var sessionManager
    @Dependency(BitkoinService.self) var bitkoinService

    public init() {}

    @ObservableState
    public struct State: Equatable {
        var seed: String = ""
        var canContinue: Bool { return !seed.isEmpty}
        
        public init() {}
        
    }


    public enum Action: ViewAction {
        case view(View)
        case delegate(DelegateAction)

        @CasePathable
        public enum View {
            case seedTextUpdated(String)
            case generateButtonTapped
            case continueButtonTapped
        }
        
        public enum DelegateAction: Equatable {
            case success
        }
    }


    public var body: some Reducer<State, Action> {
        
        Reduce { state, action in
            switch action {
            case .view(let action):
                switch action {
                case .generateButtonTapped:
                    return .run { action in
                        let newValue = try await bitkoinService.generateSeed()
                        await action(.view(.seedTextUpdated(newValue)))
                    }
                case .continueButtonTapped:
                    return .run {[seedText = state.seed] action in
                        await sessionManager.setPassphrase(seedText)
                        await action(.delegate(.success))
                    }
                    
                case .seedTextUpdated(let text):
                    state.seed = text
                    return .none
                }
            case .delegate:
                return .none
                
            }
        }
    }
}
