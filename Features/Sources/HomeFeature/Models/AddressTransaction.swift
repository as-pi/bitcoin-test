//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

public struct AddressTransaction: Equatable {
    let txId: String
    let amount: String
    let wallet: String
    let operationType: OperationType
    let date: Date
    let fee: String
    
    public enum OperationType {
        case income, outcome
    }
}

public typealias AddressTransactions = [AddressTransaction]
