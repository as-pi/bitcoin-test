//
//  HomeFeature.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import ComposableArchitecture
import SessionManager
import DesignSystem
import LogoutService
import UIKit
import SendFeature

@Reducer
public struct HomeFeature: Reducer {
    @Dependency(HomeScreenService.self) var homeScreenService
    @Dependency(SessionManager.self) var sessionManager
    @Dependency(LogoutService.self) var logoutService
    @Dependency(\.mainQueue) var mainQueue
    
    public init() {}

    @ObservableState
    public struct State: Equatable {
        
        public init() {}
        
        @Presents var sendFeature: SendFeature.State?
        var userBalance: UInt64?
        var transactions: [Date: AddressTransactions]?
        var toastMessage: String?
    }

    public enum Action: ViewAction {
        case view(View)
        case fetchData
        case fetchBalanceResult(Result<UInt64, Error>)
        case fetchTransactionsResult(Result<AddressTransactions, Error>)
        case sendFeature(PresentationAction<SendFeature.Action>)
        case updateToast(String?)
        
        @CasePathable
        public enum View {
            case onAppear
            case logoutButtonTapped
            case refreshData
            case hideToast
            case sendButtonTapped
            case copyButtonTapped
        }

    }


    public var body: some Reducer<State, Action> {
        Reduce { state, action in
            switch action {
            case .view(let action):
                switch action {
                case .onAppear:
                    return .run { action in
                        await action(.fetchData)
                    }
                case .logoutButtonTapped:
                    return .run { action in
                        await logoutService.logout()
                    }

                case .refreshData:
                    return .send(.fetchData)
                    
                case .hideToast:
                    return .run {action in
                        Task {
                            await action(.updateToast(nil))
                        }
                    }
                    
                case .sendButtonTapped:
                    state.sendFeature = .init()
                    return .none
                    
                case .copyButtonTapped:
                    return .run {action in
                        guard let outputAddress = try await homeScreenService.outputAddresses().first else { return }
                        
                        UIPasteboard.general.string = outputAddress
                        
                        await action(.updateToast("Copied"))
                        
                    }
                    
                }
            case .updateToast(let message):
                state.toastMessage = message
                return .none
                
            case .fetchData:
                return .run { action in
                    await withTaskCancellation(id: CancelID.cancel, cancelInFlight: true) {
                        
                        await action(.fetchBalanceResult(
                            Result {
                                try await homeScreenService.balance()
                            }
                        ))
                        
                        await action(.fetchTransactionsResult(
                            Result {
                                try await homeScreenService.transactions()
                            }
                        ))
                    }
                }
                
            case .fetchBalanceResult(.success(let balance)):
                state.userBalance = balance
                return .none
                
            case .fetchBalanceResult(.failure):
                return .none
                
            case .fetchTransactionsResult(.success(let transactions)):
                var data: [Date: AddressTransactions] = [:]
                for item in transactions {
                    let day = item.date.removeTimeStamp()
                    if data[day] == nil {
                        data[day] = []
                    }
                    data[day]?.append(item)
                }
                
                state.transactions = data
                
                return .none
                
            case .fetchTransactionsResult(.failure):
                return .none
                
            case .sendFeature(.presented(.delegate(let action))):
                switch action {
                case .successPayment:
                    return .send(.fetchData)
                }
                
            case .sendFeature:
                return .none
                
            }
        }
        
        .ifLet(\.$sendFeature, action: \.sendFeature) {
            SendFeature()
        }
    }
}

enum CancelID {
    case cancel
}
