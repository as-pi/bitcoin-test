//
//  HomeView.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import SwiftUI
import DesignSystem
import ComposableArchitecture
import SendFeature

@ViewAction(for: HomeFeature.self)
public struct HomeView: View {
    @Perception.Bindable public var store: StoreOf<HomeFeature>
    
    public init(store: StoreOf<HomeFeature>) {
        self.store = store
    }

    public var body: some View {
        WithPerceptionTracking(content: {
            VStack {
                contentView
            }
            .navigationStyle(title: Text(R.string.strings.homeFeatureTitle), navTitleStyle: .inline, hasBackButton: false, trailing: {
                Button {
                    send(.logoutButtonTapped)
                } label: {
                    
                    Image(systemName: "rectangle.portrait.and.arrow.right")
                }
            })
            .refreshable {
                await send(.refreshData).finish()
            }
            .task {
                await send(.onAppear).finish()
            }
            .sheet(item: $store.scope(state: \.sendFeature, action: \.sendFeature), content: {
                SendView(store: $0)
            })
        })
    }

}

#Preview {
    HomeView(store: Store(initialState: HomeFeature.State(), reducer: {
        HomeFeature()
    }))
}
