//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture

extension HomeView {
    var balanceView: some View {
        VStack {
            Image(R.image.bitcoin_logo)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 100)
            if let amount = store.userBalance {
                Text("\(amount.toBtcAmount()) " + R.string.strings.btC())
            } else {
                Text("")
            }
        }
    }
}

