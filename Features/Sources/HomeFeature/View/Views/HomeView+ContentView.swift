//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture
import UIKit

extension HomeView {
    var contentView: some View {
        VStack {
            VStack(spacing: 16) {
                balanceView
                actionsView
                historyView
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(AppTheme.Colors.backgroundPrimary)
        .toast(isPresented: .init(get: { store.toastMessage != nil }, set: {_ in send(.hideToast)})) {
            VStack {
                VStack {
                    Text(store.toastMessage ?? "")
                        .foregroundStyle(AppTheme.Colors.textPrimary)
                }
                .padding()
            }
        }
        
    }
}
