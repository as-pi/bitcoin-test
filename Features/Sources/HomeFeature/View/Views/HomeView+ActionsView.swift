//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture

extension HomeView {
    var actionsView: some View {
        VStack {
            HStack(spacing: 32) {
                Button {
                    send(.sendButtonTapped)
                } label: {
                    VStack {
                        Image(systemName: "arrow.up.to.line.circle")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .foregroundStyle(AppTheme.Colors.accentActive)
                        Text(R.string.strings.send)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.accentActive)
                    }
                }
                
                Button {
                    
                } label: {
                    VStack {
                        Image(systemName: "arrow.down.to.line.circle")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .foregroundStyle(AppTheme.Colors.accentActive)
                        Text(R.string.strings.recieve)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.accentActive)
                    }
                }
                
                Button {
                    
                    send(.copyButtonTapped)
                } label: {
                    VStack {
                        
                        Image(systemName: "doc.circle")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .foregroundStyle(AppTheme.Colors.accentActive)
                        Text(R.string.strings.copy)
                            .font(AppTheme.Fonts.bodyBold)
                            .foregroundStyle(AppTheme.Colors.accentActive)
                    }
                }
            }
            .frame(height: 64, alignment: .center)
        }
    }
}


