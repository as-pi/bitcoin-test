//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI
import DesignSystem
import ComposableArchitecture

extension HomeView {
    
    @ViewBuilder
    var historyView: some View {
        if let transactions = store.transactions {
            if transactions.count > 0 {
                    List {
                        let keys = transactions.map {$0.key}.sorted(by: {$0 > $1})
                        Section {
                            ForEach(0..<keys.count, id:\.self) {
                                sectionForDay(day: keys[$0])
                            }
                        }
                    }
                    .listStyle(.plain)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                
            } else {
                Spacer()
            }
        } else {
            Spacer()
        }
    }
    
    @ViewBuilder
    func sectionForDay(day: Date) -> some View {
        let transactionsInDay = store.transactions?[day] ?? []
        if transactionsInDay.count > 0 {
            VStack {
                
                headerView(day: day)
                
                ForEach(0..<transactionsInDay.count, id:\.self) {
                    transactionView(transaction: transactionsInDay[$0])
                }
                
            }
            .frame(maxWidth: .infinity)
            .listRowSeparator(.hidden)
            .listRowInsets(.init(top: 0, leading: 16, bottom: 16, trailing: 16))
        }
    }
    
    func headerView(day: Date) -> some View {
        let text: String
        
        if day == Date().removeTimeStamp() {
            text = R.string.strings.today()
        } else if day == Date().addingTimeInterval(-60*60*24).removeTimeStamp() {
            text = R.string.strings.yesterday()
        } else {
            let dateFormatter: DateFormatter = .init()
            dateFormatter.dateFormat = "MM.dd.yyyy"
            text = dateFormatter.string(from: day)
        }
        
        return Text(text)
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundStyle(AppTheme.Colors.textPrimary)
            .font(AppTheme.Fonts.bodyBold)
            .frame(maxWidth: .infinity)
    }
    
    func transactionView(transaction: AddressTransaction) -> some View {
        VStack(alignment: .leading) {
            HStack {
                Image(systemName: transaction.operationType == .income ? "arrow.down.to.line.circle" : "arrow.up.to.line.circle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 32)
                    .foregroundStyle(AppTheme.Colors.accentActive)
                
                VStack(alignment: .leading) {
                    Text(transaction.operationType == .income ? R.string.strings.recieved() : R.string.strings.sent())
                        .foregroundStyle(AppTheme.Colors.textPrimary)
                        .font(AppTheme.Fonts.bodyBold)
                    
                    HStack {
                        Text(transaction.operationType == .income ? "\(R.string.strings.from()):" : "\( R.string.strings.to()):")
                            .foregroundStyle(AppTheme.Colors.textPrimary)
                            .font(AppTheme.Fonts.bodyS)
                        
                        HStack {
                            Text(transaction.wallet)
                                .foregroundStyle(AppTheme.Colors.textPrimary)
                                .font(AppTheme.Fonts.bodyS)
                                .lineLimit(1)
                            
                            Text((transaction.operationType == .income ? "+" : "-") + transaction.amount)
                                .foregroundStyle(transaction.operationType == .income ? AppTheme.Colors.accentActive : AppTheme.Colors.textPrimary)
                                .font(AppTheme.Fonts.bodyS)
                        }
                    }
                    
                }
                .frame(maxWidth: .infinity)
                
            }
        }
    }
}

