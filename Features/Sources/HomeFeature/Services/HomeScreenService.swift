//
//  HomeScreenService.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import BitkoinService
import ComposableArchitecture
import BitkoinRestService
import DesignSystem

public struct HomeScreenService {
    public var balance: @Sendable () async throws -> UInt64
    public var transactions: @Sendable () async throws -> AddressTransactions
    public var outputAddresses: @Sendable () async throws -> [String]
}


extension HomeScreenService {
    public static let live: HomeScreenService = {
        
        @Dependency(BitkoinService.self) var bitkoinService
        
        return Self (
            balance: {
                let inputAddresses = try await bitkoinService.inputAddresses()
                var balance: UInt64 = 0
                
                for address in inputAddresses {
                    let data = try await BitkoinRestService.shared.getAddressData(for: address)
                    balance = balance + data.chainStats.fundedTxoSum - data.chainStats.spentTxoSum + data.mempoolStats.fundedTxoSum - data.mempoolStats.spentTxoSum
                }
                
                return balance
            },
            transactions: {
                let addresses = (try await bitkoinService.inputAddresses()) + (try await bitkoinService.outputAddresses())
                var transactions: AddressTransactions = []
                
                for address in addresses {
                    let data = try await BitkoinRestService.shared.getAddressTransactionHistory(for: address)
                    transactions.append(contentsOf: data.compactMap { data in
                        guard let lastOut = data.vout.last else { return nil }
                        
                        let operationType: AddressTransaction.OperationType = addresses.contains(lastOut.scriptpubkeyAddress) ? .outcome : .income
                        
                        let firstOuts = Array(data.vout[0..<(data.vout.count - 1)])
                        let firstOutsAmount: UInt64 = firstOuts.reduce(0, {$0 + $1.value})
                        
                        let date = data.status.blockTime ?? UInt64(Date().timeIntervalSince1970)
                        
                        return .init(
                            txId: data.txid,
                            amount: firstOutsAmount.toBtcAmount(),
                            wallet: address,
                            operationType: operationType,
                            date: .init(timeIntervalSince1970: Double(date)),
                            fee: data.fee.toBtcAmount())
                    })
                }
                
                return transactions
            },
            outputAddresses: {
                return try await bitkoinService.outputAddresses()
            }
        )
    }()
}


extension HomeScreenService: DependencyKey {
    public static let liveValue: HomeScreenService = .live
}
