//
//  MainView.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import SwiftUI
import ComposableArchitecture
import DesignSystem
import HomeFeature

public struct MainView: View {
    let store: StoreOf<MainFeature>

    public init(store: StoreOf<MainFeature>) {
        self.store = store
    }

    public var body: some View {
        WithPerceptionTracking(content: {
            TabView {
                NavigationView {
                    HomeView(store: store.scope(state: \.home,
                                                action: \.home))
                }
                .navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                    
                    Label(R.string.strings.homeTab(), systemImage: "wallet.pass")
                }


            }
            .padding(.bottom, 7)
        })
    }
}
