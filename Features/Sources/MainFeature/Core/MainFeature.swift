//
//  MainFeature.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import ComposableArchitecture
import HomeFeature

@Reducer
public struct MainFeature: Reducer {
    
    public init() {}
    
    @ObservableState
    public struct State: Equatable {
        
        var home: HomeFeature.State = .init()
        
        public init() {}
    }
    
    public enum Action {
        case home(HomeFeature.Action)
    }
    
    
    public var body: some Reducer<State, Action> {
        Reduce { state, action in
            switch action {
            case .home:
                return .none
            }
        }
        Scope(state: \.home, action: \.home) {
            HomeFeature()
        }
    }
}
