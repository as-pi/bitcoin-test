//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

// MARK: - UnspentTransactionsResponseElement
public struct UnspentTransactionsResponseItem: Codable {
    public let txid: String
    public let vout: UInt32
    public let status: Status
    public let value: UInt64
    
    // MARK: - Status
    public struct Status: Codable {
        public let confirmed: Bool
//        public let blockHeight: UInt64
//        public let blockHash: String
//        public let blockTime: UInt64

        enum CodingKeys: String, CodingKey {
            case confirmed
//            case blockHeight = "block_height"
//            case blockHash = "block_hash"
//            case blockTime = "block_time"
        }
    }
}


public typealias UnspentTransactionsResponse = [UnspentTransactionsResponseItem]
