//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

public struct AddressDataResponse: Codable {
    public let address: String
    public let chainStats, mempoolStats: Stats

    enum CodingKeys: String, CodingKey {
        case address
        case chainStats = "chain_stats"
        case mempoolStats = "mempool_stats"
    }
    
    public struct Stats: Codable {
        public let fundedTxoCount, fundedTxoSum, spentTxoCount, spentTxoSum: UInt64
        public let txCount: UInt64

        enum CodingKeys: String, CodingKey {
            case fundedTxoCount = "funded_txo_count"
            case fundedTxoSum = "funded_txo_sum"
            case spentTxoCount = "spent_txo_count"
            case spentTxoSum = "spent_txo_sum"
            case txCount = "tx_count"
        }
    }
}
/*
 {
   "address": "mngMX73gr33NsYb9AvkqT821Q6Nx12aui4",
   "chain_stats": {
     "funded_txo_count": 1,
     "funded_txo_sum": 10000,
     "spent_txo_count": 0,
     "spent_txo_sum": 0,
     "tx_count": 1
   },
   "mempool_stats": {
     "funded_txo_count": 0,
     "funded_txo_sum": 0,
     "spent_txo_count": 0,
     "spent_txo_sum": 0,
     "tx_count": 0
   }
 }
 */
