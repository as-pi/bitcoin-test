//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

// MARK: - TransactionHistoryElement
public struct AddressTransactionHistoryItem: Codable {
    public let txid: String
    public let version, locktime: UInt64
    public let vin: [Vin]
    public let vout: [Vout]
    public let size, weight, sigops, fee: UInt64
    public let status: Status
    
    // MARK: - Status
    public struct Status: Codable {
        public let confirmed: Bool
//        public let blockHeight: UInt64
//        public let blockHash: String
        public let blockTime: UInt64?

        enum CodingKeys: String, CodingKey {
            case confirmed
//            case blockHeight = "block_height"
//            case blockHash = "block_hash"
            case blockTime = "block_time"
        }
    }
    
    // MARK: - Vin
    public struct Vin: Codable {
        public let txid: String
        public let vout: UInt64
        public let prevout: Vout
        public let scriptsig, scriptsigASM: String
//        public let witness: [String]
        public let isCoinbase: Bool
        public let sequence: UInt64

        enum CodingKeys: String, CodingKey {
            case txid, vout, prevout, scriptsig
            case scriptsigASM = "scriptsig_asm"
//            case witness
            case isCoinbase = "is_coinbase"
            case sequence
        }
    }

    // MARK: - Vout
    public struct Vout: Codable {
        public let scriptpubkey, scriptpubkeyASM, scriptpubkeyType, scriptpubkeyAddress: String
        public let value: UInt64

        enum CodingKeys: String, CodingKey {
            case scriptpubkey
            case scriptpubkeyASM = "scriptpubkey_asm"
            case scriptpubkeyType = "scriptpubkey_type"
            case scriptpubkeyAddress = "scriptpubkey_address"
            case value
        }
    }
}

public typealias AddressTransactionHistory = [AddressTransactionHistoryItem]
