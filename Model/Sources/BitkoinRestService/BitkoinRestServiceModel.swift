//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

public protocol BitkoinRestServiceModel {
    
    func getAddressData(for address: String) async throws -> AddressDataResponse
    func getAddressTransactionHistory(for address: String) async throws -> AddressTransactionHistory
    func getUTXO(for address: String) async throws -> UnspentTransactionsResponse
    func createTransaction(hex: String) async throws -> String
    
}
