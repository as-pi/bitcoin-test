//
//  File.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation

public class BitkoinRestService: BitkoinRestServiceModel {
    private var endpoint: URL = .init(string: "https://mempool.space/")!
    private var urlSession: URLSession = {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil

        let session = URLSession(configuration: config)
        return session
    }()
    
    private(set) public static var shared: BitkoinRestServiceModel = BitkoinRestService()
    
    private func getData<T: Decodable>(for request: URLRequest) async throws -> T {
        let data = try await urlSession.data(for: request)
        
        if T.self == String.self, let stringValue = String(data: data.0, encoding: .utf8), let stringValue = stringValue as? T {
            return stringValue
        } else {
            do {
                return try JSONDecoder().decode(T.self, from: data.0)
            } catch {
                print("error: \(error)")
                throw error
            }
        }
    }
    
    public func getAddressData(for address: String) async throws -> AddressDataResponse {
        let request: URLRequest = .init(url: endpoint
            .appendingPathComponent("signet")
            .appendingPathComponent("api")
            .appendingPathComponent("address")
            .appendingPathComponent(address)
        )
        
        return try await getData(for: request)
    }
    
    public func getAddressTransactionHistory(for address: String) async throws -> AddressTransactionHistory {
        let request: URLRequest = .init(url: endpoint
            .appendingPathComponent("signet")
            .appendingPathComponent("api")
            .appendingPathComponent("address")
            .appendingPathComponent(address)
            .appendingPathComponent("txs")
        )
        
        return try await getData(for: request)
    }
    
    public func getUTXO(for address: String) async throws -> UnspentTransactionsResponse {
        let request: URLRequest = .init(url: endpoint
            .appendingPathComponent("signet")
            .appendingPathComponent("api")
            .appendingPathComponent("address")
            .appendingPathComponent(address)
            .appendingPathComponent("utxo")
        )
        return try await getData(for: request)
    }
    
    public func createTransaction(hex: String) async throws -> String {
        var request: URLRequest = .init(url: endpoint
            .appendingPathComponent("signet")
            .appendingPathComponent("api")
            .appendingPathComponent("tx")
        )
        request.httpMethod = "POST"
        request.httpBody = hex.data(using: .utf8)
        let result:String = try await getData(for: request)
        
        if !result.isEmpty, !result.contains(" ") {
            return result
        } else {
            throw ServiceError(error: result)
        }
    }
    
    struct ServiceError: LocalizedError {
        let error: String
        
        var errorDescription: String? { return error}
    }
}
