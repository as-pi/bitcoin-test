//
//  SessionManager.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import Dependencies
import KeychainClient

public struct SessionManager {
    enum Key: String, CaseIterable {
        
        case isLoggedIn
        
    }
    
    enum KeychainCoding: String {
        case passcode
    }
    
    // MARK: - User Session Data

    public var isNeedLogin: @Sendable () -> Bool
    public var passphrase: @Sendable () -> String?
    public var setPassphrase: @Sendable (_ key: String) async -> Void
    
    // MARK: - Session handling
    
    public var clearSessionData: @Sendable () async -> Void
}


extension SessionManager: DependencyKey {
    
    public static let liveValue: SessionManager = {
        @Dependency(KeychainClient.self) var keychain

        return Self(
            isNeedLogin: {
                return keychain.read(KeychainCoding.passcode.rawValue)?.isEmpty != false
            },
            passphrase: {
                keychain.read(KeychainCoding.passcode.rawValue)
            },
            setPassphrase: { key in
                await keychain.update(KeychainCoding.passcode.rawValue, key)
            },
            clearSessionData: {
                try? await keychain.deleteAll()
            }
        )
    }()
}

extension DependencyValues {
    public var sessionManager: SessionManager {
        get { self[SessionManager.self] }
        set { self[SessionManager.self] = newValue }
    }
}
