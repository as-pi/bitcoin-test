//
//  LogoutService.swift
//
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import ComposableArchitecture
import Combine

public class LogoutService: ObservableObject {
    @Published public private(set) var loggedOut: Bool = false
    private var cancellable: AnyCancellable?
    
    public var sessionStream: AsyncStream<Bool> {
        return AsyncStream { continuation in
            cancellable = self.$loggedOut.sink { loggedOut in
                continuation.yield(loggedOut)
            }
            
            continuation.onTermination = { @Sendable _ in
                self.cancellable = nil
                self.loggedOut = false
            }
        }
    }
    
    public func logout() async {
        self.loggedOut = true
    }
}

extension LogoutService: DependencyKey {
    public static let liveValue: LogoutService = LogoutService()
}

extension LogoutService: TestDependencyKey {
    public static let testValue: LogoutService = LogoutService()
}
