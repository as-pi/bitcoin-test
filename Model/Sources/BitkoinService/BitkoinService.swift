//
//  File.swift
//  
//
//  Created by aleksey on 16.06.2024.
//

import Foundation
import ComposableArchitecture
import BitcoinKit
import SessionManager

public struct BitkoinService {
    
    public struct GenerateHexData {
        
        public init(fromWallet: String, toWallet: String, amount: UInt64, fee: UInt64, walletUtxoData: [WalletUtxoData]) {
            self.fromWallet = fromWallet
            self.toWallet = toWallet
            self.amount = amount
            self.fee = fee
            self.walletUtxoData = walletUtxoData
        }
        
        public let fromWallet: String
        public let toWallet: String
        public let amount: UInt64
        public let fee: UInt64
        public let walletUtxoData: [WalletUtxoData]
        
        public struct WalletUtxoData {
            
            public init(balance: UInt64, txID: String, index: UInt32) {
                self.balance = balance
                self.txID = txID
                self.index = index
            }
            
            public let balance: UInt64
            public let txID: String
            public let index: UInt32
        }
    }
    
    public var generateSeed: @Sendable () async throws -> String
    
    public var outputAddresses: @Sendable () async throws -> [String]
    public var inputAddresses: @Sendable () async throws -> [String]
    
    public var calculateComission: @Sendable (Int) async throws -> UInt64
    
    public var generateHexForTransaction: @Sendable (GenerateHexData) async throws -> String // address: balance
}

extension BitkoinService: DependencyKey {
    public static let liveValue: BitkoinService = {
        
        @Dependency(SessionManager.self) var sessionManager
        
        return Self (
            generateSeed: {
                return try Mnemonic.generate().joined(separator: " ")
            },
            outputAddresses: {
                guard let key = sessionManager.passphrase() else { throw ServiceError.emptyKey }
                let words: [String] = key.split(separator: " ").map { String($0) }
                let seed = Mnemonic.seed(mnemonic: words, validateChecksum: {_ in })
                
                let hdWallet = HDWallet.init(seed: seed, externalIndex: 0, internalIndex: 0, network: .testnetBTC)
                return hdWallet.internalAddresses.map { $0.legacy }
            },
            inputAddresses: {
                guard let key = sessionManager.passphrase() else { throw ServiceError.emptyKey }
                let words: [String] = key.split(separator: " ").map { String($0) }
                let seed = Mnemonic.seed(mnemonic: words, validateChecksum: {_ in })
                
                let hdWallet = HDWallet.init(seed: seed, externalIndex: 0, internalIndex: 0, network: .testnetBTC)
                return hdWallet.externalAddresses.map { $0.legacy }
            },
            calculateComission: { inputsCount in
                
                let feePerByte: UInt64 = 1
                let fee = FeeCalculator.calculateFee(inputs: UInt64(inputsCount), outputs: 2, feePerByte: feePerByte)
                return fee
                
            },
            generateHexForTransaction: { data in
                
                let totalBalance = data.walletUtxoData.reduce(0, {$0 + $1.balance})
                
                guard totalBalance > data.amount + data.fee else {
                    throw ServiceError.invalidAmount
                }
                
                guard let key = sessionManager.passphrase() else { throw ServiceError.emptyKey }
                let words: [String] = key.split(separator: " ").map { String($0) }
                let seed = Mnemonic.seed(mnemonic: words, validateChecksum: {_ in })
                
                let hdWallet = HDWallet.init(seed: seed, externalIndex: 0, internalIndex: 0, network: .testnetBTC)
                let toAddress: BitcoinAddress = try! BitcoinAddress(legacy: data.toWallet)
                
                guard let (walletIndex, walletFrom) = hdWallet.addresses.enumerated().first(where: {$0.element.legacy == data.fromWallet}) else { throw ServiceError.emptyKey }
                
                let walletPrivateKey = hdWallet.privKeys[walletIndex]
                var unspentTransactions:[UnspentTransaction] = []
                let lockScript = Script(address: walletFrom)!.data
                
                
                
                for item in data.walletUtxoData {
                    let prevTxID = item.txID
                    let hash = Data(Data(hex: prevTxID)!.reversed())
                    let index: UInt32 = item.index
                    let outpoint = TransactionOutPoint(hash: hash, index: index)
                    
                    let output = TransactionOutput(value: item.balance, lockingScript: lockScript)
                    let unspentTransaction = UnspentTransaction(output: output, outpoint: outpoint)
                    
                    unspentTransactions.append(unspentTransaction)
                }
                
                let plan = TransactionPlan(unspentTransactions: unspentTransactions, amount: data.amount, fee: data.fee, change: totalBalance - data.amount - data.fee)
                let transaction = TransactionBuilder.build(from: plan, toAddress: toAddress, changeAddress: walletFrom)
                
                let signer = TransactionSigner(unspentTransactions: unspentTransactions, transaction: transaction, sighashHelper: BTCSignatureHashHelper(hashType: .ALL))
                let signedTransaction = try! signer.sign(with: [walletPrivateKey])
                return signedTransaction.serialized().hex
            }
        )
    }()
}

extension BitkoinService {
    enum ServiceError: LocalizedError {
        case emptyKey
        case invalidAmount
        
        var errorDescription: String? {
            switch self {
            case .emptyKey:
                return "emptyKey"
            case .invalidAmount:
                return "invalidAmount"
            }
        }
    }
}
