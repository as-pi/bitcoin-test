//
//  KeychainClient.swift
//  
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import KeychainAccess
import Dependencies


public struct KeychainClient {
    public var read: @Sendable (String) -> String?
    public var update: @Sendable (String, String) async -> Void
    public var delete: @Sendable (String) async throws -> Void
    public var deleteAll: @Sendable () async throws -> Void
}

extension KeychainClient: DependencyKey {
    
    public static let liveValue: Self = {
        let keychain = Keychain(service: "group.bitcoin-test.private")
        
        return Self(read: { keychain[$0] },
                    update: { keychain[$0] = $1 },
                    delete: { try keychain.remove($0) },
                    deleteAll: { try keychain.removeAll() })
    }()
}
