// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Model",
    platforms: [
      .iOS(.v15),
      .macOS(.v10_15),
    ],
    products: [
        .library(
            name: "SessionManager",
            targets: ["SessionManager"]),
        .library(
            name: "KeychainClient",
            targets: ["KeychainClient"]),
//        .library(
//            name: "Utilities",
//            targets: ["Utilities"]),
        .library(
            name: "LogoutService",
            targets: ["LogoutService"]),
        .library(
            name: "BitkoinService",
            targets: ["BitkoinService"]),
        .library(
            name: "BitkoinRestService",
            targets: ["BitkoinRestService"]),
    ],
    dependencies: [
        .package(url: "https://github.com/pointfreeco/swift-composable-architecture", .upToNextMinor(from: "1.10.4")),
        .package(url: "https://github.com/kishikawakatsumi/KeychainAccess", .upToNextMinor(from: "4.2.2")),
        .package(url: "https://github.com/sorifico/BitcoinKit", branch: "master"),
    ],
    targets: [
        .target(
            name: "LogoutService",
            dependencies: [
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
            ]),
        .target(
            name: "SessionManager",
            dependencies: [
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
                "KeychainClient",
//                "Utilities",
            ]),
        .target(
            name: "KeychainClient",
            dependencies: [
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
                .product(name: "KeychainAccess", package: "KeychainAccess"),
            ]),
//        .target(
//            name: "Utilities",
//            dependencies: [
//                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
//            ]),
        .target(
            name: "BitkoinService",
            dependencies: [
                .product(name: "BitcoinKit", package: "BitcoinKit"),
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture"),
                "SessionManager"
            ]),
        .target(
            name: "BitkoinRestService",
            dependencies: [
                
            ])
    ]
)
