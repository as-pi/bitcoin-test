//
//  AppDelegate.swift
//  VD.Pay
//
//  Created by aleksey on 17.06.2024.
//

import Foundation
import SwiftUI
import AppFeature
import ComposableArchitecture

class AppDelegate: NSObject, UIApplicationDelegate {
    
    let store = Store(initialState: AppFeature.State(), reducer: {
        AppFeature()
    })
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        store.send(.appDelegate(.didFinishLaunching))
        return true
    }
    
}
