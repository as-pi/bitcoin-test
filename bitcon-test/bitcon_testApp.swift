//
//  bitcon_testApp.swift
//  bitcon-test
//
//  Created by aleksey on 15.06.2024.
//

import SwiftUI
import AppFeature

@main
struct bitcon_testApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {
        WindowGroup {
            AppView(store: appDelegate.store)
                .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
                    appDelegate.store.send(.appWillEnterForeground)
                }
        }
    }
}
